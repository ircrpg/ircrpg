package org.fealdia.ircclient

import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.fealdia.ircclient.IRCParser._
import org.fealdia.ircclient.IRCParser.Message
import org.fealdia.ircclient.IRCParser.NickPrefix
import org.fealdia.ircclient.IRCParser.ServerPrefix

class IRCParserSpec extends FlatSpec with ShouldMatchers {
  behavior of "servername parser"

  it should "parse server name" in {
    IRCParser.parseAll(IRCParser.servername, "moorcock.freenode.net").get should equal ("moorcock.freenode.net")
  }

  behavior of "prefix parser"

  it should "parse serverprefix properly" in {
    IRCParser.parseAll(IRCParser.prefix, "moorcock.freenode.net").get should equal (ServerPrefix("moorcock.freenode.net"))
  }

  it should "parse nickprefix properly" in {
    IRCParser.parseAll(IRCParser.prefix, "hoxu!~hoxu@unaffiliated/hoxu").get should equal (NickPrefix("hoxu", "~hoxu", "unaffiliated/hoxu"))
  }

  behavior of "params parser"

  it should "parse trailing parameter" in {
    checkResult(IRCParser.params, " :word1 word2", Array("word1 word2"))
  }

  it should "parse two word parameters" in {
    checkResult(IRCParser.params, " param1 param2", Array("param1", "param2"))
  }

  it should "parse middle and trailing parameter" in {
    checkResult(IRCParser.params, " * :*** Looking up your hostname...", Array("*", "*** Looking up your hostname..."))
  }

  // FIXME
  behavior of "message parser"

  it should "parse dummy line with one param" in {
    checkResult(IRCParser.message, ":moorcock.freenode.net NOTICE :not real", Message(ServerPrefix("moorcock.freenode.net"), "NOTICE", Array("not real")))
  }

  it should "parse server notice" in {
    checkResult(IRCParser.message, ":moorcock.freenode.net NOTICE * :*** Looking up your hostname...",
      Message(ServerPrefix("moorcock.freenode.net"), "NOTICE", Array("*", "*** Looking up your hostname...")))
  }

  it should "parse numeric commands" in {
    checkResult(IRCParser.message, ":hubbard.freenode.net 004 darthago hubbard.freenode.net ircd-seven-1.1.3 DOQRSZaghilopswz CFILMPQbcefgijklmnopqrstvz bkloveqjfI",
      Message(ServerPrefix("hubbard.freenode.net"), "004", "hubbard.freenode.net ircd-seven-1.1.3 DOQRSZaghilopswz CFILMPQbcefgijklmnopqrstvz bkloveqjfI".split(" ").toArray))
  }

  def checkResult[T](parser: IRCParser.Parser[T], input: String, output: T) {
    IRCParser.parseAll(parser, input) match {
      case IRCParser.Success(res, next) => res should equal (output)
      case IRCParser.Failure(msg, next) => {
        //println(msg)
        throw new RuntimeException(msg)
      }
      case IRCParser.Error(msg, _) => throw new RuntimeException(msg)
    }
  }
}
