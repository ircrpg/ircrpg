package org.fealdia.ircclient

import util.parsing.combinator.RegexParsers

// See RFC1459 for IRC protocol details
// :moorcock.freenode.net NOTICE * :*** Looking up your hostname...
// :darthago MODE darthago :+i

/* RFC1459 section 2.3.1
<message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
<prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
<command>  ::= <letter> { <letter> } | <number> <number> <number>
<SPACE>    ::= ' ' { ' ' }
<params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]

<middle>   ::= <Any *non-empty* sequence of octets not including SPACE
or NUL or CR or LF, the first of which may not be ':'>
<trailing> ::= <Any, possibly *empty*, sequence of octets not including
NUL or CR or LF>

<crlf>     ::= CR LF
*/


object IRCParser extends RegexParsers {
  override def skipWhitespace = false

  abstract class Prefix
  case class ServerPrefix(servername: String) extends Prefix
  case class NickPrefix(nick: String, user: String, host: String) extends Prefix {
    def fullMask: String = nick + '!' + user + '@' + host
  }

  case class Message(prefix: Prefix, command: String, params: Array[String]) {
    override def toString: String = "Message[" + prefix + " --- " + command + " --- " + params.mkString(",") + "]"
  }

  // See RFC1459, section 2.3.1
  def message: Parser[Message] = opt( ":" ~> prefix <~ space) ~ command ~ params ~ "$".r ^^ {
    case pref ~ cmd ~ par ~ eol => pref match {
      case Some(pref) => new Message(pref, cmd, par)
      case _ => new Message(null, cmd, par)
    }
  }
  def prefix: Parser[Prefix] = servername ^^ { ServerPrefix(_) } | nick ~ opt("!" ~> user) ~ opt("@" ~> host) ^^ {
    case n ~ u ~ h => NickPrefix(n, u.getOrElse(null), h.getOrElse(null))
  }
  def command: Parser[String] = "[A-Za-z]+|[0-9]{3}".r
  def space = " +".r
  def params: Parser[Array[String]] = space ~> opt(":" ~ trailing | middle ~ opt(params)) ^^ {
    case Some(o) => o match {
      case ":" ~ (trail: String) => Array[String](trail)
      case (m: String) ~ Some((p: Array[String])) => Array[String](m) ++ p
      case (m: String) ~ None => Array[String](m)
    }
    case None => Array[String]("noparams")
  }
  def middle: Parser[String] = "[^ :][^ ]*".r
  def trailing: Parser[String] = ".*".r

  def servername = "[^ !@.]+\\.[^ !@]+".r
  def host = "[^ !@]+".r // TODO RFC 952?
  def nick: Parser[String] = regex(letter) ~ rep1(letter | number | special) ^^ { case a ~ b => (a :: b).mkString }

  def user: Parser[String] = "[^ @]+".r
  def letter = "[A-Za-z]".r
  def number = "[0-9]".r
  def special = """[-\[\]\\`^\{\}]""".r // TODO [] should be included

  def parseMessage(str: String): ParseResult[Message] = parseAll(message, str)

  def main(args: Array[String]) {
    println("Parser test")

    val lines = List(
      ":moorcock.freenode.net NOTICE * :*** Looking up your hostname...",
      ":hoxu!~hoxu@unaffiliated/hoxu PRIVMSG #example :hello world"
    )

    for (l <- lines) {
      println("<= " + l)
      println("=> " + parseMessage(l) + "\n")
    }
  }
}
