package org.fealdia.ircclient

import collection.mutable
import org.fealdia.ircclient.IRCParser.NickPrefix

trait SessionData {
  var channelNicks = mutable.Map.empty[String, mutable.Set[String]].withDefaultValue(mutable.Set.empty)
  var fullMasks = mutable.Map.empty[String, String] // nick -> fullMask

  def addChannelUser(channel: String, nick: NickPrefix) = {
    channelNicks(channel) = channelNicks(channel) + nick.nick
    addFullMask(nick.nick, nick.fullMask)
  }
  def addChannelUsers(channel: String, users: TraversableOnce[String]) = channelNicks(channel) = channelNicks(channel) ++ users
  def addFullMask(nick: String, fullMask: String) = {
    // If no mask at all, or if the match has changed
    if (!fullMasks.contains(nick) || !(fullMasks.get(nick).get equals fullMask)) {
      fullMasks.put(nick, fullMask)
      onFullMaskAdded(nick, fullMask)
    }
  }
  def channels = channelNicks.keys
  def onFullMaskAdded(nick: String, fullMask: String) {}
  def removeChannelUser(channel: String, user: String) = {
    channelNicks(channel).remove(user)
    fullMasks.remove(user)
  }
  def removeChannel(channel: String) = channelNicks.remove(channel)
}
