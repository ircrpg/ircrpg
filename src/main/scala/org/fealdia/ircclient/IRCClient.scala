package org.fealdia.ircclient

import java.net.Socket
import java.io.{PrintWriter, BufferedReader, InputStreamReader}
import org.fealdia.ircclient.IRCParser.{Message, NickPrefix}
import org.apache.log4j.Logger

class IRCClient extends SessionData {
  lazy val log = Logger.getLogger(this.getClass)

  var socket: Socket = null
  var out: PrintWriter = null

  // Config
  var nick = "ircclient"

  type Message = IRCParser.Message

  def connect(hostname: String, port: Int) {
    socket = new Socket(hostname, port)
    out = new PrintWriter(socket.getOutputStream(), true)

    sendLine("NICK " + nick)
    sendLine("USER darthago hostname servername :RPG bot")
  }

  def join(channel: String) {
    sendLine("JOIN " + channel)
  }

  protected def onChannelJoin(who: NickPrefix, channel: String) {
    addChannelUser(channel, who)

    log.debug(s"channelNicks ${channelNicks}")
  }

  protected def onChannelPart(who: NickPrefix, channel: String) {
    val Nick = nick
    who.nick match {
      case Nick => {
        removeChannel(channel)
      }
      case n => removeChannelUser(channel, n)
    }
    log.debug(s"channelNicks ${channelNicks}")
  }

  protected def onCommandReceived(message: Message) {
    //println("=== " + prefix + " " + command + " " + params)

    message.command match {
      case "JOIN" => {
        message.prefix match {
          case np: NickPrefix => onChannelJoin(np, message.params(0))
          case _ => log.warn("Unexpected JOIN prefix")
        }
      }
      case "NOTICE" => None
      case "PART" => message.prefix match {
        case np: NickPrefix => onChannelPart(np, message.params(0))
      }
      case "PING" => sendLine("PONG " + message.params(0))
      case "PRIVMSG" => {
        message.prefix match {
          case np: NickPrefix => onPrivMsg(np, message.params(0), message.params(1))
        }
      }
      // :gibson.freenode.net 353 darthago @ #channel :darthago pippa supie
      case "353" => { // RPL_NAMREPLY
        val channel = message.params(2)
        val nicks = message.params(3).split(" ")
        addChannelUsers(channel, nicks)

        log.debug("Channelnicks: " + channelNicks)
      }
      // Message of the Day, meant for human comsumption, so ignore
      case "375" => None // RPL_MOTDSTART
      case "372" => None // RPL_MOTD
      case "376" => None // RPL_ENDOFMOTD
      case _ => log.warn("Unhandled message: " + message)
    }
  }

  protected def onLineReceived(line: String) {
    log.debug("<<< " + line)

    val msg = IRCParser.parseMessage(line).get

    try {
      onCommandReceived(msg)
    } catch {
      case e: Exception => log.error("Exception while handling line", e)
    }
  }

  protected def onPrivMsg(who: NickPrefix, where: String, message: String) {
    addFullMask(who.nick, who.fullMask)
  }

  def run() {
    val in = new BufferedReader(new InputStreamReader(socket.getInputStream))

    Stream.continually(in.readLine()).takeWhile(_ != null) foreach { line => onLineReceived(line) }
    log.info("run() exiting")
  }

  def privMsg(target: String, message: String) {
    sendLine("PRIVMSG " + target + " :" + message)
  }

  def sendLine(line: String) {
    out.write(line + "\r\n")
    out.flush()
    log.debug(">>> " + line)
  }
}
