package org.fealdia.rpgbot.content

import util.Random

abstract class Monster(val name: String, val experience: Int, val damage: (Int,Int)) {
  def rollDamage = {
    new Random().nextInt(damage._2 - damage._1) + damage._1
  }
}

object Bat extends Monster("bat", 2, (1, 2))
object Rat extends Monster("rat", 3, (2, 3))
object Bear extends Monster("bear", 4, (3, 5))
