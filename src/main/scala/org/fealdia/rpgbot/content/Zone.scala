package org.fealdia.rpgbot.content

abstract class Zone(val name: String, val monsters: List[Monster])

object Hamlet extends Zone("hamlet", List(Bat, Rat, Bear))
