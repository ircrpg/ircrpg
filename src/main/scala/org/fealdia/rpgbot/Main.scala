package org.fealdia.rpgbot

import database.GameDB
import org.springframework.jdbc.datasource.DriverManagerDataSource

object Main extends App {
  // Set up database
  Class.forName("org.sqlite.JDBC")
  val dataSource = new DriverManagerDataSource("jdbc:sqlite:ircrpg.sqlite")
  GameDB.dataSource = dataSource

  GameDB.getPlayer("test")

  val bot = new RPGBot()

  bot.start() // Actor
  bot.run() // Socket listener
}