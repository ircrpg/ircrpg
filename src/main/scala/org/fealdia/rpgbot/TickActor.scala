package org.fealdia.rpgbot

import actors.Actor

// TODO synchronize the ticks to clock
class TickActor(var timeout: Long, val target: Actor) extends Actor {
  case object Tick

  def act() {
    loop {
      reactWithin(timeout) {
        case _ => target ! Tick
      }
    }
  }
}
