package org.fealdia.rpgbot.database

import javax.sql.DataSource
import org.springframework.jdbc.core.{RowMapper, JdbcTemplate}
import java.sql.ResultSet

object GameDB {
  var dataSource: DataSource = null

  def tpl = {
    new JdbcTemplate(dataSource)
  }

  object PlayerMapper extends RowMapper[Player] {
    def mapRow(rs: ResultSet, rowNum: Int) = {
      new Player(rs.getString("name"), rs.getString("password"), rs.getInt("ap"), rs.getInt("hp"), rs.getInt("maxhp"), rs.getInt("xp"), rs.getInt("gold"))
    }
  }

  def addMask(player: String, fullMask: String) {
    tpl.update("INSERT INTO mask(player_id, mask) SELECT id,? FROM player WHERE name = ?", fullMask, player)
  }

  def checkPassword(name: String, password: String): Boolean = {
    getPlayer(name) match {
      case Some(player) => player.password.equals(password)
      case None => false
    }
  }

  def decrementAP(player: String, amount: Integer = 1): Boolean = {
    val res = tpl.update("UPDATE player SET ap=ap-? WHERE name = ? AND ap >= ?", amount, player, amount)
    res > 0
  }

  def decrementHP(player: String, amount: Integer) {
    tpl.update("UPDATE player SET hp=hp-? WHERE name=?", amount, player)
  }

  def getPlayer(name: String): Option[Player] = {
    val result = tpl.query("SELECT * FROM player WHERE name = ?", Array[Object](name), PlayerMapper)

    result.size() match {
      case 0 => None
      case _ => Some(result.get(0))
    }
  }

  def getPlayerByMask(fullMask: String): Option[Player] = {
    val result = tpl.query("SELECT player.* FROM player, mask WHERE mask.player_id=player.id AND mask = ?", Array[Object](fullMask), PlayerMapper)

    result.size() match {
      case 0 => None
      case _ => Some(result.get(0))
    }
  }

  def incrementActionPoints(amount: Int, maximum: Int) {
    tpl.update("UPDATE player SET ap=min(ap+?, ?)", amount.asInstanceOf[Integer], maximum.asInstanceOf[Integer])
  }

  def incrementHPByPercentage(percentage: Int) {
    tpl.update("UPDATE player SET hp=min(hp+CAST(10*(?/100.0) AS INTEGER), maxhp)", percentage.asInstanceOf[Integer])
  }

  def incrementPlayerGold(player: String, amount: Int) {
    tpl.update("UPDATE player SET gold=gold+? WHERE name = ?", amount.asInstanceOf[Integer], player)
  }

  def incrementPlayerHP(player: String, amount: Int) {
    tpl.update("UPDATE player SET hp=min(hp+?, maxhp) WHERE name = ?", amount.asInstanceOf[Integer], player)
  }

  def incrementXP(player: String, amount: Int) {
    tpl.update("UPDATE player SET xp=xp+? WHERE name=?", amount.asInstanceOf[Integer], player)
  }

  def killPlayer(name: String) {
    tpl.update("UPDATE player SET ap=0 WHERE name = ?", name)
  }

  def registerPlayer(name: String, password: String) {
    tpl.update("INSERT INTO player(name, password) VALUES(?, ?)", name, password)
  }
}
