package org.fealdia.rpgbot.database

class Player(val name: String, val password: String, val ap: Int, val hp: Int, val maxhp: Int, val xp: Int, val gold: Int)
