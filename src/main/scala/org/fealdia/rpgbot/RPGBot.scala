package org.fealdia.rpgbot

import collection.mutable
import content.Hamlet
import database.GameDB
import org.fealdia.ircclient.IRCClient
import org.fealdia.ircclient.IRCParser.NickPrefix
import actors.Actor
import util.Random

class RPGBot extends IRCClient with Actor {
  val gamedb = GameDB
  val tickActor = new TickActor(120000, this)

  val nickToPlayer = mutable.Map.empty[String, String]

  // Config
  val prefix = '!'
  val maxActionPoints = 20

  sealed abstract class Command(val cmd: String, val playerRequired: Boolean, val requiredParams: Int = 0) {
    val help: String
    val params: String = ""
    def handleCommand(who: String, params: List[String]): List[String]
  }

  object AddMaskCommand extends Command("addmask", true) {
    val help = "Add your full nick!user@host mask to the bot for automatically identifying"
    def handleCommand(who: String, params: List[String]) = {
      val mask = fullMasks(who)
      gamedb.addMask(who, mask)
      List(s"Mask ${mask} added")
    }
  }

  object HelpCommand extends Command("help", false) {
    val help = "Shows a list of commands"
    def handleCommand(who: String, params: List[String]) = {
      for (command <- commands) yield s"${command.cmd} ${command.params} - ${command.help}"
    }
  }

  object HuntCommand extends Command("hunt", true, 0) {
    val help = "Go hunting in the wilds. Requires 1 AP"
    def handleCommand(who: String, params: List[String]) = {
      if (!gamedb.decrementAP(who)) {
        List("No AP")
      } else {
        // Get random mob from zone
        val mob = Random.shuffle(Hamlet.monsters).head
        val healthLost = mob.rollDamage
        val xp = mob.experience

        val pl = gamedb.getPlayer(who).get
        if (pl.hp <= healthLost) {
          gamedb.killPlayer(who)
          List(s"You were severely injured by a ${mob.name} while hunting, and need to rest for a while")
        } else {
          gamedb.decrementHP(who, healthLost)

          // Increment experience
          gamedb.incrementXP(who, xp)

          // Increment gold
          val coins = if (new Random().nextInt(5) <= 1) 1 else 0
          gamedb.incrementPlayerGold(who, coins)

          List(s"You killed a ${mob.name}, and gained ${xp} XP, ${coins} coins, lost ${healthLost} HP")
        }
      }
    }
  }

  object IdentifyCommand extends Command("identify", false, 1) {
    val help = "Identify yourself to the bot"
    override val params = "<password>"
    def handleCommand(who: String, params: List[String]): List[String] = {
      if (gamedb.checkPassword(who, params(0))) {
        nickToPlayer(who) = who
        List("Password correct")
      } else {
        List("Wrong password")
      }
    }
  }

  object PubCommand extends Command("pub", true) {
    val help = "Visit local pub"
    def handleCommand(who: String, params: List[String]) = {
      if (!gamedb.decrementAP(who)) {
        List("No AP")
      } else {
        // TODO + reputation, - gold
        List("You wasted some time at the local pub")
      }
    }
  }

  object RegisterCommand extends Command("register", false, 1) {
    val help = "Create a new player"
    override val params = "<password>"
    def handleCommand(who: String, params: List[String]) = {
      log.info("Register called")

      // check that player does not exist yet
      gamedb.getPlayer(who) match {
        case Some(pl) => List("Player already exists")
        case _ => {
          // Add mask to autoidentify
          AddMaskCommand.handleCommand(who, Nil)

          try {
            gamedb.registerPlayer(who, params(0))
            List(s"New player created. /msg ${nick} identify <password> to identify")
          } catch {
            case e: Exception => List("Failed to register player")
          }
        }
      }
    }
  }

  object RestCommand extends Command("rest", true) {
    val help = "Visit local inn to rest properly"
    def handleCommand(who: String, params: List[String]) = {
      if (!gamedb.decrementAP(who)) {
        List("No AP")
      } else {
        val maxhp = gamedb.getPlayer(who).get.maxhp
        gamedb.incrementPlayerHP(who, maxhp / 2)
        List("You take a long nap at the local inn, and wake up feeling well rested")
      }
    }
  }

  // TODO admin-only command
  object SetTickCommand extends Command("settick", true) {
    val help = "Change server tick rate, in seconds"
    override val params = "<tick interval>"

    def handleCommand(who: String, params: List[String]) = {
      val interval = params(0).toInt
      if (interval < 120) {
        List("Tick interval 120 is minimum")
      } else {
        tickActor.timeout = interval * 1000
        List("Tick interval changed to " + interval)
      }
    }
  }

  object StatusCommand extends Command("status", true) {
    val help = "Show status of your player character"
    def handleCommand(who: String, params: List[String]): List[String] = {
      val p = gamedb.getPlayer(who).get
      List(s"Player ${p.name} has ${p.ap}/${maxActionPoints} action points, ${p.hp}/${p.maxhp} health, ${p.xp} XP, ${p.gold} gold")
    }
  }

  val commands = List(AddMaskCommand, HelpCommand, HuntCommand, IdentifyCommand, PubCommand, RegisterCommand, RestCommand, SetTickCommand, StatusCommand)

  def act() {
    loop {
      react {
        // New line from server
        case line: String => super.onLineReceived(line)
        // Tick from timer
        case _ => tick
      }
    }
  }

  def onBotAddressed(from: String, where: String, message: String) {
    log.info("Bot addressed: " + from + " - " + message)

    val params = message.split(" +")

    val messages: List[String] = commands.find(_.cmd equals params(0)) match { // Find matching command
      case Some(command) => {
        if (command.requiredParams > params.length) {
          List("Not enough parameters for command")
        } else command.playerRequired match { // Does the command require player?
          case true => nickToPlayer.get(from) match { // Do we have a player?
            case Some(player) => command.handleCommand(player, params.tail.toList)
            case None => List("That command requires you to be identified")
          }
          case false => command.handleCommand(from, params.tail.toList)
        }
      }
      case None => List("Unrecognized command: " + message)
    }

    // Now return the "result" messages
    val Nick = nick
    where match {
      // Private messages back to private
      case Nick => messages.map(m => privMsg(from, m))
      // Channel messages address the nick
      case _ => messages.map(m => privMsg(where, from + ": " + m))
    }
  }

  override protected def onChannelJoin(who: NickPrefix, channel: String) {
    super.onChannelJoin(who, channel)

    val Nick = nick
    who.nick match {
      case Nick => privMsg(channel, "RPGBot active! \"/msg %s register <password>\" to register".format(nick))
      case _ => {
        // Check if full mask matches player, if so auto-identify
        gamedb.getPlayerByMask(who.fullMask) match {
          case Some(player) => {
            nickToPlayer(who.nick) = player.name
            privMsg(channel, s"${who.nick}: Welcome back! You have been identified automatically by your mask")
          }
          case None => privMsg(channel, s"${who.nick}: Welcome to IRCRPG! '/msg $nick register <password>' to register and start playing")
        }
      }
    }
  }

  override protected def onChannelPart(who: NickPrefix, channel: String) {
    super.onChannelPart(who, channel)

    nickToPlayer.remove(who.nick)
  }

  override def onFullMaskAdded(nick: String, fullMask: String) {
    log.debug(s"Full mask added for ${nick} - ${fullMask}")

    gamedb.getPlayerByMask(fullMask) match {
      case Some(player) => nickToPlayer(nick) = player.name
      case None => {}
    }
  }

  override protected def onLineReceived(line: String) {
    // Actor will relay the line to super.onLineReceived
    this ! line
  }

  override protected def onPrivMsg(from: NickPrefix, where: String, message: String) {
    super.onPrivMsg(from, where, message)

    val nick = from.nick
    log.info("PRIVMSG " + nick + " -> " + where + ": " + message)

    // check for command prefix, or nick
    if (where equals nick) {
      onBotAddressed(nick, where, message)
    }
    else if (message.startsWith(nick + ": ")) {
      onBotAddressed(nick, where, message.substring(nick.length + 1))
    }
    else if (message(0) equals prefix) {
      onBotAddressed(nick, where, message.substring(1))
    }
  }

  override def run() {
    nick = "darthago"
    connect("chat.freenode.net", 6667)
    join("#darthago")

    // start a timer thread, to increase AP
    tickActor.start()

    super.run()
  }

  def sayOnAllChannels(text: String) {
    for (channel <- channels) {
      privMsg(channel, text)
    }
  }

  def tick {
    log.debug("Tick")
    sayOnAllChannels("Tick! All players gain +1 AP, +20% HP")
    gamedb.incrementActionPoints(1, maxActionPoints)
    gamedb.incrementHPByPercentage(20)
  }
}
