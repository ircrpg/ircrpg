name := "ircrpg"

version := "0.1.0"

scalaVersion := "2.10.0"

libraryDependencies += "org.scala-lang" % "scala-actors" % "2.10.0"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test"

libraryDependencies += "org.springframework" % "spring-jdbc" % "3.2.0.RELEASE"

libraryDependencies += "org.xerial" % "sqlite-jdbc" % "3.7.2"
