[tech]

[game]
- EQ2 research-like focus targets
	- you focus on something for N ticks, and then gain it? Stats?
- pub: give new objectives for game?
- objectives: picked at random, replaced when finished, 3 active ones?
	- win 5 consequtive battles
	- hyperactive: spend all AP for five consequtive ticks
	- drunk: visit the pub 5 times in row
	- stingy: keep some AP reserves for N ticks
	- lurker: play on five consequtive days
- leveling - 200 exp = level 2?
- password hashing
- bonus for playing consequtive days
- player: add ap_used, ap_cumulated
- Achievements
	- could give points, that can be spent to gain some permanent bonus (eg. maxap+1)
Player
    Actions_spent
    Inventory?
    Zone?
Actions
    Travel zone
    Shop?
    Auction house
Non-actions
    Inventory/equip changes
World events (every “tick”)
    Tick = every real time hour
    Players on some zones (outdoors?) may get attacked, or get lucky and win something in town for example
NPCs/shops?
    Permanent shops that keep player items?
Player death?